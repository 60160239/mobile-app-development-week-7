import 'package:flutter/material.dart';

class CheckboxTileWidget extends StatefulWidget {
  CheckboxTileWidget({Key? key}) : super(key: key);

  @override
  _CheckboxTileWidgetState createState() => _CheckboxTileWidgetState();
}

class _CheckboxTileWidgetState extends State<CheckboxTileWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Container(
       child: Scaffold(
         appBar: AppBar(title: Text('CheckboxTile')),
         body: ListView(
           children: [
             CheckboxListTile(
               title: Text('Check 1'),
               subtitle: Text('To Check 1'),
               value: check1, 
               onChanged: (value){
                 setState(() {
                  check1 = value!;
                 });
               }),
               CheckboxListTile(
               title: Text('Check 2'),
               subtitle: Text('To Check 2'),
               value: check2, 
               onChanged: (value){
                 setState(() {
                  check2 = value!;
                 });
               }),
               CheckboxListTile(
               title: Text('Check 3'),
               subtitle: Text('To Check 3'),
               value: check3, 
               onChanged: (value){
                 setState(() {
                  check3 = value!;
                 });
               }),
               TextButton.icon(onPressed: (){ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('check 1 = $check1, check2 = $check2, check3 = $check3')));}, icon: Icon(Icons.save), label: Text('Save'))
           ],
         ),
       )
    );
  }
}