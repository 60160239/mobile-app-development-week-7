import 'package:flutter/material.dart';

class dropdownWidget extends StatefulWidget {
  dropdownWidget({Key? key}) : super(key: key);

  @override
  _dropdownWidgetState createState() => _dropdownWidgetState();
}

class _dropdownWidgetState extends State<dropdownWidget> {
  String vaccine = '-';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Dropdown'),),
      body: Column(
        children: [
          Container(
            child: Row(
              children: [
                Text('Vaccine'),
                Expanded(child: Container()),
                DropdownButton(
                  items: [
                    DropdownMenuItem(child: Text('-'),value: '-'),
                    DropdownMenuItem(child: Text('Pfizer'),value: 'Pfizer'),
                    DropdownMenuItem(child: Text('Johnson & Johnson'),value: 'Johnson & Johnson'),
                    DropdownMenuItem(child: Text('Sputnik V'),value: 'Sputnik V'),
                    DropdownMenuItem(child: Text('Astrazenaca'),value: 'Astrazenaca'),
                    DropdownMenuItem(child: Text('Novavax'),value: 'Novavax'),
                    DropdownMenuItem(child: Text('Sinopharm'),value: 'Sinopharm'),
                    DropdownMenuItem(child: Text('Sinovac'),value: 'Sinovac')
                  ],
                  value: vaccine,
                  onChanged: (String? newValue){
                    setState(() {
                      vaccine = newValue!;
                    });
                  },
                  )
              ],
            ),
          ),
          Center(
            child: Text(vaccine, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
          )
        ],
      ),
    );
  }
}