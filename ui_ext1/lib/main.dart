import 'package:flutter/material.dart';
import 'package:ui_ext1/checkboxtilewid.dart';
import 'package:ui_ext1/checkboxwid.dart';
import 'package:ui_ext1/dropdownwid.dart';
import 'package:ui_ext1/radiowid.dart';

void main() {
  runApp(MaterialApp(
    title: 'UI EXT',
    home: MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Ui Ext'),),
        drawer: Drawer(
          child: ListView(
            children: [
              const DrawerHeader(
                child: Text('UI Menu'),
                decoration: BoxDecoration(
                  color: Colors.blue
                ),

              ),
              ListTile(
              title: Text('CheckBox'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => CheckboxWidget()));
              },
            ),
            ListTile(
              title: Text('CheckBoxTile'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => CheckboxTileWidget()));
              },
            ),
            ListTile(
              title: Text('Dropdown'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => dropdownWidget()));
              },
            ),
            ListTile(
              title: Text('Radio'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => RadioWidget()));
              },
            ),
            ],
          ),
        ),
        body: ListView(
          children: [
            ListTile(
              title: Text('CheckBox'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => CheckboxWidget()));
              },
            ),
            ListTile(
              title: Text('CheckBoxTile'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => CheckboxTileWidget()));
              },
            ),
            ListTile(
              title: Text('Dropdown'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => dropdownWidget()));
              },
            ),
            ListTile(
              title: Text('Radio'),
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => RadioWidget()));
              },
            ),
          ],
        ),
      );
  }
}
